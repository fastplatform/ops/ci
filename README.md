# Common assets for Continuous Integration

## GitLab

This repository contains Gitlab CI jobs definitions that can be included remotely:
```yaml
include:
- project: gitlab/project/path
  file: /gitlab/common.yml
```