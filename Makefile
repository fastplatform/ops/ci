# HELP
# This will output the help for each task
# thanks to https://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
.PHONY: help

help: ## This help.
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

.DEFAULT_GOAL := help

CI_REGISTRY=registry.gitlab.com/fastplatform/ops/ci


# Publish Docker images
.PHONY: publish-docker-images
publish: ## Publish all Docker images used in the Kustomize bases
	$(eval TARGET_REGISTRY := $(CI_REGISTRY))
	@find . -type f -name Makefile -mindepth 2  | \
	  xargs -n 1 sh -c 'grep "mirror-docker-images:" $$0 2>&1 >/dev/null && dirname $$0 || exit 0' | \
	  xargs -n 1 sh -c 'cd $$0 && $(MAKE) mirror-docker-images'
